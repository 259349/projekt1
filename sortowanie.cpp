#include "sortowanie.h"

template < typename T >

  void merge(T * tab, int l, int m, int p) {
    int roz_l = m - l + 1; //rozmiary nowych tablic
    int roz_p = p - m;

    int Ltab[roz_l];
    int Ptab[roz_p];
    for (int i = 0; i < roz_l; i++) {
      Ltab[i] = tab[l + i]; //do podtablicy przypisuje elementy starej tablicy (od wyznaczonego momentu), tak żeby byo od początku
    }
    for (int j = 0; j < roz_p; j++) {
      Ptab[j] = tab[m + 1 + j];
    }
    int i = 0;
    int j = 0;
    int k = l;

    while (i < roz_l && j < roz_p) {
      if (Ltab[i] <= Ptab[j]) {
        tab[k] = Ltab[i]; //tam gdzie się stara skończyła, to przypisuje element z lewej podtablicy
        i++;
      } else {
        tab[k] = Ptab[j]; //a inaczej to z prawej
        j++;
      }
      k++;
    }
    while (i < roz_l) { //kopiowanie pozostałych elementów, jak np. j>roz_p a wciąż i<roz_l
      tab[k] = Ltab[i];
      i++;
      k++;
    }
    while (j < roz_p) {
      tab[k] = Ptab[j];
      j++;
      k++;
    }
  }

template < typename T >
  void mergeSort(T * tab, int l, int p) {
    int m;
    if (l < p) {
      m = (p + l) / 2; //dzielenie tablicy na pół
      mergeSort(tab, l, m); //sortowanie lewej tablicy
      mergeSort(tab, m + 1, p); //sortowanie prawej tablicy
      merge(tab, l, m, p); //scalanie
    }
  }

/* Quick Sort */

//funkcja szuka punktu w którym podzieli talice, tzw. pivot
template < typename T >
  int podzial(T * tab, int l, int p) {
    int pivot = tab[(l + p) / 2]; //ustawia pivot w połowie
    int i = l;
    int j = p;

    while (1) {
      while (tab[j] > pivot) j--; //szukanie elementu większego od pivota stojącego po lewej od pivota

      while (tab[i] < pivot) i++; //szukanie elementu mniejszego od pivota, który stoi po prawej od pivota

      if (i < j) {
        std::swap(tab[i++], tab[j--]); //kiedy się przetną, to zamiana miejsc tak długo jak i<j
      } else {
        return j;
      }
    }
  }

template < typename T >
  void quickSort(T * tab, int l, int p) {
    if (l < p) {
      int n = podzial(tab, l, p); //przypisuje j
      quickSort(tab, l, n); //sortuje lewą część
      quickSort(tab, n + 1, p); //sortuje prawą część + 1,  żeby tego samego elementu nie sortować
    }
  }


/*Sortowanie przez wstawianie*/
template < typename T >
  void insertionsort(T * tab, int l, int p) {
    for (int i = l + 1; i <= p; i++) //od drugiego elementu tablicy
    {
      int k = tab[i]; //ustawienie klucza
      int j = i;

      while (j > l && tab[j - 1] > k) //dopóki to jest następny element i ten element jest większy od klucza. Czy po lewej od klucza jest mniejszy
      {
        tab[j] = tab[j - 1]; //zamiana miejscami
        j--;
      }
      tab[j] = k;
    }
  }


/*Heap sort*/
template < typename T >
  void heapsort(T * l, T * p) {
    std::make_heap(l, p);
    std::sort_heap(l, p);
  }

/*Intro Sort*/
template < typename T >
  void introSort(T * tab, T * l, T * p, int glebokosc) {
    if ((p - l) < 9) insertionsort(tab, l - tab, p - tab); //wywolanie sortowania przez wstawianie
    else if (glebokosc == 0) heapsort(l, p + 1); //wywołanie przez kopcowniae
    else {
      int pivot = podzial(tab, l - tab, p - tab); //w innych introsort    
      introSort(tab, l, tab + pivot, glebokosc - 1);
      introSort(tab, tab + pivot + 1, p, glebokosc - 1);
    }
  }

/////////////////////////////////////////////////////////////

template void mergeSort < int > (int * , int, int);
template void quickSort < int > (int * , int, int);
template void introSort < int > (int * , int * , int * , int);