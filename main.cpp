#include "sortowanie.h"

#include <iostream>

#include <chrono>

#include <math.h>


template < typename T >
  void wypelnij_losowo(T * tab, int n) {
    for (int i = 0; i < n; i++) {
      tab[i] = rand() % 1000 + 1;
      //  std::cout<<tab[i]<<std::endl;
    }
    //std::cout<<"Koniec tablicy randomowej"<<std::endl;

  }

template < typename T >
  void usun_tablice(T * tab) {
    delete[] tab;
  }

//sortuje odpowiedni procent danych na początku tablicy
template < typename T >
  void posortowane_procent(T * tab, int n, double procent) {
    if (procent < 100) {
      std::sort(tab, tab + (int)((n * procent) / 100)); //sortuje do tego procentu
    } else {
      std::sort(tab, tab + n, std::greater < T > ()); //sortuje  100%, ale od najwyższej do najniższej
      return;
    }
  }

template < typename T >
  void sprawdz(T * tab, int n) {
    for (int i = 0; i < n - 1; i++) {
      if (tab[i] > tab[i + 1]) {
        std::cout << "Tablica nie jest posortowana!" << std::endl;
      }
    }
  }

template < typename T >
  double sort_merge(int n, T * tab) {
    auto czas_p = std::chrono::high_resolution_clock::now(); //wystartowanie czasu
    mergeSort(tab, 0, n - 1);
    auto czas_k = std::chrono::high_resolution_clock::now(); //stop czas
    return std::chrono::duration < double, std::milli > (czas_k - czas_p).count(); //zwraca różnicę końca z początkiem
  }

template < typename T >
  double sort_quick(int n, T * tab) {
    auto czas_p = std::chrono::high_resolution_clock::now();
    quickSort(tab, 0, n - 1);
    auto czas_k = std::chrono::high_resolution_clock::now();
    return std::chrono::duration < double, std::milli > (czas_k - czas_p).count();
  }

template < typename T >
  double sort_intro(T * tab, int r, int max) {
    auto czas_p = std::chrono::high_resolution_clock::now();
    introSort(tab, tab, tab + r, max);
    auto czas_k = std::chrono::high_resolution_clock::now();

    return std::chrono::duration < double, std::milli > (czas_k - czas_p).count();
  }

int main() {
  srand(time(NULL));
  int t = 0;
  int ilosc = 5;
  int rozmiar_tab[5] = {10000, 50000, 100000, 500000, 1000000};
  //int rozmiar_tab[5]={2,3,4,5,6};
  double procenty_tab[8] = {0, 25, 50, 75, 95, 99, 99.7, 100};
  int ilosc_procent = 8;
  int ilosc_tablic = 5;
  int rozmiar;
  double czas = 0;
  double zliczanie = 0;
  double procent = 0;

  std::cout << "Merge Sort" << std::endl;
  for (int i = 0; i < ilosc_procent; i++) {
    procent = procenty_tab[i];
    for (int j = 0; j < ilosc_tablic; j++) {
      rozmiar = rozmiar_tab[j];
      int * tab = new int[rozmiar];

      for (int i = 0; i < ilosc; i++) {
        wypelnij_losowo < int > (tab, rozmiar);
        posortowane_procent < int > (tab, rozmiar, procent);
        zliczanie = sort_merge < int > (rozmiar, tab);
        /*std::cout<<"Tablica posortowana: "<<std::endl;
                for (int k = 0; k < rozmiar; k++){
                    std::cout<<tab[k]<<std::endl;}
                std::cout<<"Koniec tablicy posortowanej"<<std::endl;*/
        sprawdz < int > (tab, rozmiar);
        czas = czas + zliczanie;
      }
      std::cout << czas / ilosc << " ";
      czas = 0;
      usun_tablice < int > (tab);
      std::cout << rozmiar << " elementowa tablica posortowana!" << std::endl;
    }
    std::cout << procent << " % posortowane!" << std::endl;
  }

  std::cout << "Quick Sort" << std::endl;
  for (int i = 0; i < ilosc_procent; i++) {
    procent = procenty_tab[i];
    for (int j = 0; j < ilosc_tablic; j++) {
      rozmiar = rozmiar_tab[j];
      int * tab = new int[rozmiar];

      for (int i = 0; i < ilosc; i++) {
        wypelnij_losowo < int > (tab, rozmiar);
        posortowane_procent < int > (tab, rozmiar, procent);
        zliczanie = sort_quick < int > (rozmiar, tab);
        /*for (int k = 0; k < rozmiar; k++){
                    std::cout<<tab[k]<<std::endl;}
                std::cout<<"Koniec tablicy posortowanej"<<std::endl;*/
        sprawdz < int > (tab, rozmiar);
        czas = czas + zliczanie;
      }
      std::cout << czas / ilosc << " ";
      czas = 0;
      usun_tablice < int > (tab);
      std::cout << rozmiar << " elementowa tablica posortowana!" << std::endl;
    }
    std::cout << procent << " % posortowane!" << std::endl;
  }

  std::cout << "Intro Sort" << std::endl;
  for (int i = 0; i < ilosc_procent; i++) {
    procent = procenty_tab[i];
    for (int j = 0; j < ilosc_tablic; j++) {
      rozmiar = rozmiar_tab[j];
      int * tab = new int[rozmiar];

      for (int i = 0; i < ilosc; i++) {
        wypelnij_losowo < int > (tab, rozmiar);
        posortowane_procent < int > (tab, rozmiar, procent);
        zliczanie = sort_intro < int > (tab, rozmiar - 1, log(rozmiar) * 2);
        /*for (int k = 0; k < rozmiar; k++){
                    std::cout<<tab[k]<<std::endl;}
                std::cout<<"Koniec tablicy posortowanej"<<std::endl;*/
        sprawdz < int > (tab, rozmiar);
        czas = czas + zliczanie;
      }
      std::cout << czas / ilosc << " "; //saves the average time spent on sorting a single table
      czas = 0;
      usun_tablice < int > (tab);
      std::cout << rozmiar << " elementowa tablica posortowana!" << std::endl;
    }
    std::cout << procent << " % posortowane!" << std::endl;
  }
  return 0;
}