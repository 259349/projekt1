#ifndef SORTOWANIE_H
#define SORTOWANIE_H

#include <algorithm>


template<typename T>
void mergeSort(T *tab, int l, int p);

template<typename T>
void quickSort(T *tab,  int l, int p);

template<typename T>
void introSort(T *tab, T *l, T *p, int glebokosc);

#endif
